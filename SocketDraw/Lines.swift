//
//  Lines.swift
//  SocketDraw
//
//  Created by Joseph Marrero on 4/26/15.
//  Copyright (c) 2015 Joseph Marrero. All rights reserved.
//

import UIKit

class Lines {
    

    var start: CGPoint
    var end: CGPoint
    
    init( start _start: CGPoint, end _end: CGPoint){
        start = _start
        end = _end
    }
    

}