//
//  ViewController.swift
//  SocketDraw
//
//  Created by Joseph Marrero on 4/23/15.
//  Copyright (c) 2015 Joseph Marrero. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBOutlet weak var drawPanel: DrawPanel!

    
    //Clear by settings lines to empty and redraw panel
    @IBAction func eraseBtn(sender: AnyObject) {
        drawPanel.lines = []
        drawPanel.setNeedsDisplay()
    }
    
    
    //TODO save ViewController (Panel) to png
    @IBAction func submitBtn(sender: AnyObject) {
        //
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0.0)
        view.drawViewHierarchyInRect(view.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        
      
        
        
        var refreshAlert = UIAlertController(title: "Save", message: "Saving Drawing to Photo Library \n and Clearing Canvas", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
           
            UIImageWriteToSavedPhotosAlbum(image, self,
                "image:didFinishSavingWithError:contextInfo:", nil)
            
            self.drawPanel.lines = []
            self.drawPanel.setNeedsDisplay()
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
            //Do nothing
        }))
        
        presentViewController(refreshAlert, animated: true, completion: nil)
                //
       
    }
    
    func image(image: UIImage, didFinishSavingWithError
        error: NSErrorPointer, contextInfo:UnsafePointer<Void>) {
            
            if error != nil {
                // Report error to user
            }
    }
    
    
}

