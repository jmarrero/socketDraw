//
//  DrawPanel.swift
//  SocketDraw
//
//  Created by Joseph Marrero on 4/23/15.
//  Copyright (c) 2015 Joseph Marrero. All rights reserved.
//

import UIKit

class DrawPanel: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    var lines: [Lines] = []
    var lastPoint: CGPoint!
    
     required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        //self.backgroundColor = UIColor.blackColor()
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
    var touch = touches.first as! UITouch
    lastPoint = touch.locationInView(self)
    }
    
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent){
        var touch = touches.first as! UITouch
        var newPoint = touch.locationInView(self)
        lines.append(Lines(start: lastPoint, end: newPoint))
        lastPoint = newPoint
        
        self.setNeedsDisplay() //redraw
    }
    
    override func drawRect(rect: CGRect) {
        var context = UIGraphicsGetCurrentContext()
        CGContextBeginPath(context)
        for line in lines {
            CGContextMoveToPoint(context, line.start.x, line.start.y)
            CGContextAddLineToPoint(context, line.end.x, line.end.y)
        }
        CGContextSetLineCap(context, kCGLineCapRound)
        CGContextSetRGBStrokeColor(context, 0,0,0,1)
        CGContextSetLineWidth(context, 3)
        CGContextStrokePath(context)
    }
    
}
